class RandomStubs

  ## Medicines has three properties: name, volume (in mg), price (price per tablet)

  RANDOM_MEDS = [{
                     "name" => "Ibuprofen",
                     "price" => 389,
                     "volume" => 250
                 }, {
                     "name" => "Gabapentin",
                     "price" => 241,
                     "volume" => 150
                 }, {
                     "name" => "OXYGEN",
                     "price" => 189,
                     "volume" => 150
                 }, {
                     "name" => "Amoxicillin",
                     "price" => 165,
                     "volume" => 150
                 }, {
                     "name" => "Hydrochlorothiazide",
                     "price" => 157,
                     "volume" => 150
                 }, {
                     "name" => "Naproxen",
                     "price" => 146,
                     "volume" => 250
                 }, {
                     "name" => "Hand Sanitizer",
                     "price" => 143,
                     "volume" => 150
                 }, {
                     "name" => "Metformin Hydrochloride",
                     "price" => 142,
                     "volume" => 200
                 }, {
                     "name" => "Lisinopril",
                     "price" => 138,
                     "volume" => 200
                 }, {
                     "name" => "Levetiracetam",
                     "price" => 138,
                     "volume" => 250
                 }, {
                     "name" => "Omeprazole",
                     "price" => 128,
                     "volume" => 250
                 }, {
                     "name" => "Lorazepam",
                     "price" => 127,
                     "volume" => 250
                 }, {
                     "name" => "Metronidazole",
                     "price" => 126,
                     "volume" => 250
                 }, {
                     "name" => "Cyclobenzaprine Hydrochloride",
                     "price" => 125,
                     "volume" => 200
                 }, {
                     "name" => "Atenolol",
                     "price" => 125,
                     "volume" => 250
                 }]

  ## Tests has three properties name, description, key_metrics

  RANDOM_TESTS = [{
                      "name" => "Amniocentesis",
                      "desc" => "Analysis of fluid, removed by a needle inserted through the abdominal wall, to detect an abnormality in the fetus"
                  }, {
                      "name" => "Arteriography (angiography)",
                      "desc" => "X-ray study using radiopaque dye injected through a thin tube (catheter), which is threaded to the artery being studied, to detect and outline or highlight a blockage or defect in an artery"
                  }, {
                      "name" => "Blood pressure measurement",
                      "desc" => "Test for high or low blood pressure, usually using an inflatable cuff wrapped around the arm"
                  }, {
                      "name" => "Blood tests",
                      "desc" => "Measurement of substances in the blood to evaluate organ function and to help diagnose and monitor various disorders"
                  }, {
                      "name" => "Bone marrow aspiration",
                      "desc" => "Removal of a bone marrow sample by a needle for examination under a microscope to check for abnormalities in blood cells"
                  }, {
                      "name" => "Bronchoscopy",
                      "desc" => "Direct examination with a viewing tube to check for a tumor or other abnormality"
                  }, {
                      "name" => "Dual x-ray absorptiometry (DEXA)",
                      "desc" => "Low-dose x-ray study to determine the thickness of bones"
                  }, {
                      "name" => "Echocardiography",
                      "desc" => "Study of heart structure and function using sound waves"
                  }, {
                      "name" => "Electrocardiography (ECG)",
                      "desc" => "Study of the heart’s electrical activity using electrodes attached to the arms, legs, and chest"
                  }, {
                      "name" => "Electroencephalography (EEG)",
                      "desc" => "Study of the brain’s electrical function using electrodes attached to the scalp"
                  }, {
                      "name" => "Mammography",
                      "desc" => "X-ray study to check for breast cancer"
                  }, {
                      "name" => "Paracentesis",
                      "desc" => "Insertion of a needle into the abdominal cavity to remove fluid for examination"
                  }, {
                      "name" => "Percutaneous transhepatic cholangiography",
                      "desc" => "X-ray study of the liver and biliary tract after a radiopaque dye is injected into the liver"
                  }, {
                      "name" => "Positron emission tomography (PET)",
                      "desc" => "Imaging test using particles that release radiation (positrons) to detect abnormalities in function"
                  }, {
                      "name" => "Venography",
                      "desc" => "X-ray study using a radiopaque dye (similar to arteriography) to detect blockage of a vein"
                  }]

  KEY_METRICS = [
      "KM 1",
      "KM 2",
      "KM 3",
      "KM 4",
      "KM 5",
      "KM 6",
      "KM 7"
  ]

  def generate_random_prescription(count)
    frequency = APP_CONFIG['med_timings']
    rand_meds = RANDOM_MEDS.sample(count)
    rand_meds.each do |medicine|
      timing = frequency[frequency.keys.sample]
      medicine.merge!({"timing" => timing})
    end
    return rand_meds.to_json
  end

  def generate_random_test
    rand_test =  RANDOM_TESTS.sample
    rand_test['key_metrics'] = {}
    rand_km = KEY_METRICS.sample(rand(2..7))
    rand_km.each do |keym|
      rand_test['key_metrics'].merge!({keym => "#{rand(256..798)} units"})
    end
    return rand_test
  end

end