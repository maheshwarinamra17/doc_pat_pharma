class AuthenticateUser

  attr_accessor :email, :password, :headers

  # Encode & Decode token

  def encode(payload, exp = 3.hours.from_now)
    payload[:exp] = exp.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  end

  def decode(token)
    body = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    HashWithIndifferentAccess.new body
  rescue Exception => e
    nil
  end

  # Authenticating a user

  def get_user
    auth_user = TbUser.find_by_email_id(email)
    ## Check error email problem
    if(!auth_user.present? || !auth_user.authenticate(password))
      auth_user = {}
      auth_user["errors"] = Array.new()
      auth_user["errors"].push(I18n.t 'errors.e_credentials')
    end
    return auth_user
  end

  def get_auth_token
    result = {}
    user = get_user
    if(user['errors'].present?)
      result['auth_token'] = nil
      result['errors'] = user['errors']
    else
      result['auth_token'] = encode(user_id: user.id)
    end
    return result
  end

  # Authorizing API Request

  def get_authorized_user
    result = {}
    decoded_token = decode_auth_token
    if decoded_token
      begin
        result['auth_user'] = TbUser.find(decoded_token[:user_id])
      rescue Exception => e
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_token')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_token')
    end
    return result
  end

  def decode_auth_token
    result = decode(http_auth_header)
    return result
  end

  def http_auth_header
    result = nil
    if headers['Authorization'].present?
      result = headers['Authorization'].split(' ').last
    end
    return result
  end

end
