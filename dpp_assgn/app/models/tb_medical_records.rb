class TbMedicalRecords < ApplicationRecord

  def self.create_a_medical_record params
    result = {}
    constraint = TbUser.exists?(:email_id => params[:email_id], :role => APP_CONFIG['user_type']['patient'])
    if params[:doc_access_json].present? and params[:doc_access_json]['source'].present?
      constraint = (constraint and TbUser.exists?(:id => params[:doc_access_json]['source'], :role => APP_CONFIG['user_type']['doctor']))
    end
    if constraint
      pat_user = TbUser.where(:email_id => params['email_id'])
      params[:patient_user_id] = pat_user.first[:id]
      params[:doc_access_json] = params[:doc_access_json].to_json.to_s
      result = TbMedicalRecords.create(params.permit(:patient_user_id, :med_test_type, :med_test_json, :doc_access_json))
      if result.present?
        my_pres = JSON.parse(pat_user.first[:extra_params_json])
        my_pres['mid'] << result[:id]
        pat_user.update(:extra_params_json => my_pres.to_json.to_s)
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_illegal')
    end
    return result
  rescue Exception => e
    result = nil
  end

  ## doctor can choose from various records of patients
  def self.request_a_med_rec params, user
    result = TbMedicalRecords.where(:id => params['id'], :patient_user_id => params['patient_user_id']).first
    if result.present?
      col_name = :doc_access_json
      access_json = JSON.parse(result[col_name])
      access_json['others'][user['id']]= {
          "doc_status" => APP_CONFIG['doc_status']['pending'],
      }
      result.update(col_name => access_json.to_json.to_s)
      req_user = TbUser.where(:id => user['id']).first
      my_med_rec = JSON.parse(user[:extra_params_json])
      if(!my_med_rec['mid'].include?result[:id])
        my_med_rec['mid'] << result[:id]
      end
      req_user.update(:extra_params_json => my_med_rec.to_json.to_s)
      result = {"id" => result[:id], "request_user_id" => user['id'], "doc_status" => APP_CONFIG['doc_status']['pending']}
    end
    return result
  rescue Exception => e
    result = nil
  end

  ## Patient cannot approve if doctor doesnot request
  def self.approve_a_med_rec user_id, med_rec
    result = {}
    user = TbUser.find(user_id)
    if user.present? and user['role'] = APP_CONFIG['user_type']['doctor']
      col_name = :doc_access_json
      if !med_rec[col_name].present?
        result = nil
        return result
      end
      access_json = JSON.parse(med_rec[col_name])
      if access_json['others'].present? and access_json['others'][user['id'].to_s].present? and access_json['others'][user['id'].to_s]['doc_status'].present?
        access_json['others'][user['id'].to_s] = {
            "doc_status" => APP_CONFIG['doc_status']['approved'],
            "timestamp" => Time.now
        }
        med_rec.update(col_name => access_json.to_json.to_s)
        result = {"id" => med_rec[:id], "request_user_id" => user['id'], "doc_status" => APP_CONFIG['doc_status']['approved']}
      else
        result = nil
        return result
      end
    end
    return result
  rescue Exception => e
    result = nil
  end

  def self.get_med_records med_rec_ids, user_id
    result = {}
    if med_rec_ids.kind_of?(Array)
      result = TbMedicalRecords.where(:id => med_rec_ids).select(:id, :med_test_type, :doc_access_json)
      result.each do |med_rec|
        med_rec[:doc_access_json] = JSON.parse(med_rec[:doc_access_json])['others'][user_id.to_s]['doc_status']
      end
    end
    return result
  rescue Exception => e
    result = nil
  end

  def self.get_med_rec_data params, user
    result = TbMedicalRecords.where(:id => params['id'])
    if result.present?
      result = result.select(:id, :patient_user_id, :med_test_json, :updated_at, :doc_access_json).first
    end
    result[:doc_access_json] = JSON.parse(result[:doc_access_json])['others'][user['id'].to_s]['doc_status']
    return result
  rescue Exception => e
    result = nil
  end

end