class TbPrescription < ApplicationRecord

  def self.create_a_prescription params
    pat_user = TbUser.where(:email_id => params['email_id'])
    params[:patient_user_id] = pat_user.first[:id]
    constraint_1 = TbUser.exists?(:id => params[:patient_user_id], :role => APP_CONFIG['user_type']['patient'])
    constraint_2 = TbUser.exists?(:id => params[:doctor_user_id], :role => APP_CONFIG['user_type']['doctor'])
    if constraint_1 and constraint_2
      result = TbPrescription.create(params.permit(:patient_user_id, :doctor_user_id, :drugs_json))
      if result.present?
        my_pres = JSON.parse(pat_user.first[:extra_params_json])
        my_pres['pid'] << result[:id]
        pat_user.update(:extra_params_json => my_pres.to_json.to_s)
      end
    else
      result = {}
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_illegal')
    end
    return result
  rescue Exception => e
    result = nil
  end

  def self.request_a_prescription params, user
    result = TbPrescription.where(:id => params['id'], :patient_user_id => params['patient_user_id']).first
    if result.present?
      col_name = :pharma_access
      if user['role'] == APP_CONFIG['user_type']['doctor']
        col_name = :doc_access
      end
      if !result[col_name].present?
        result[col_name] = "{}"
      end
      access_json = JSON.parse(result[col_name])
      access_json[user['id']]= {
          "doc_status" => APP_CONFIG['doc_status']['pending']
      }
      result.update(col_name => access_json.to_json.to_s)
      req_user = TbUser.where(:id => user['id']).first
      my_pres = JSON.parse(user[:extra_params_json])
      if(!my_pres['pid'].include?result[:id])
        my_pres['pid'] << result[:id]
      end
      req_user.update(:extra_params_json => my_pres.to_json.to_s)
      result = {"id" => result[:id], "request_user_id" => user['id'], "doc_status" => APP_CONFIG['doc_status']['pending']}
    end
    return result
  rescue Exception => e
    result = nil
  end

  ## Patient cannot approve if pharmacist or doctor doesnot request
  def self.approve_a_prescription user_id, prescription
    result = {}
    user = TbUser.where(:id => user_id).first
    if user.present?
      col_name = :pharma_access
      if user['role'] == APP_CONFIG['user_type']['doctor']
        col_name = :doc_access
      end
      if !prescription[col_name].present?
        result = nil
        return result
      end
      access_json = JSON.parse(prescription[col_name])
      if access_json[user['id'].to_s].present? and  access_json[user['id'].to_s]['doc_status'].present?
        access_json[user['id'].to_s] = {
            "doc_status" => APP_CONFIG['doc_status']['approved'],
            "timestamp" => Time.now
        }
        prescription.update(col_name => access_json.to_json.to_s)
        result = {"id" => prescription[:id], "request_user_id" => user['id'],"doc_status" => APP_CONFIG['doc_status']['approved']}
      else
        result = nil
        return result
      end
    end
    return result
  rescue Exception => e
    result = nil
  end

  def self.get_prescription pres_ids, user
    result = {}
    col_name = :pharma_access
    if user['role'] == APP_CONFIG['user_type']['doctor']
      col_name = :doc_access
    end
    if pres_ids.kind_of?(Array)
      result = TbPrescription.where(:id => pres_ids).select(:id, :updated_at, col_name)
      result.each do |pres|
        pres[col_name] =  JSON.parse(pres[col_name])[user['id'].to_s]['doc_status']
      end
    end
    return result
  rescue Exception => e
    result = nil
  end

  def self.get_prescription_data params, user
    result = TbPrescription.where(:id => params['id'])
    col_name = :pharma_access
    if user['role'] == APP_CONFIG['user_type']['doctor']
      col_name = :doc_access
    end
    if result.present?
      result = result.select(:id, :patient_user_id, :drugs_json, :updated_at, col_name).first
    end
    result[col_name] = JSON.parse(result[col_name])[user['id'].to_s]['doc_status']
    return result
  rescue Exception => e
    result = nil
  end

end