class TbUser < ApplicationRecord

  has_secure_password
  validates :email_id, :uniqueness => {:case_sensitive => false}

  def self.create_a_new_user new_user_params
    new_user_params[:extra_params_json] = {
        "pid" => [],
        "mid" => []
    }.to_json.to_s
    result = TbUser.create(new_user_params.permit(:name, :mobile, :email_id, :password, :password_confirmation, :role, :extra_params_json))
  rescue Exception => e
    result = nil
  end

end