class ApplicationController < ActionController::Base
  # protect_from_forgery with: :exception

  attr_reader :current_user

  private

  def authenticate_api_request
    auth_user = AuthenticateUser.new
    auth_user.headers = request.headers
    user = auth_user.get_authorized_user
    if(user['auth_user'].present?)
      @current_user = user['auth_user']
    else
      result = {}
      result['errors'] = Array.new()
      result['errors'] = user['errors']
      render json: result, status: 200
    end
  end


end
