class UserController < ApplicationController

  before_action :authenticate_api_request, :except => [:authenticate_user, :create_a_new_user]

  def authenticate_user
    auth_user = AuthenticateUser.new
    auth_user.email = params[:email_id]
    auth_user.password = params[:password]
    result = auth_user.get_auth_token
    render :json => result
  end

  def create_a_new_user
    result = {}
    if params[:name].present? and params[:email_id].present? and  params[:password].present?
      params[:role] = APP_CONFIG['user_type'][params[:role]]
      if params[:role].present?
        new_user = TbUser.create_a_new_user(params)
        if new_user.present? and new_user[:id].present?
          result['user_sign_up'] = true
          result['email_id'] = new_user[:email_id]
        else
          if(!result['errors'])
            result['errors'] = Array.new
          end
          result['errors'].push(I18n.t 'errors.e_reg_email')
        end
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_role')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_uname')
    end
    render :json => result and return
  end

  ## only for doctors and pharmacists
  def get_prescriptions
    result = {}
    pres_ids  = JSON.parse(@current_user[:extra_params_json])['pid']
    if pres_ids.present?
      result = TbPrescription.get_prescription(pres_ids,@current_user)
      if !result.present?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_no_rec')
    end
    render :json => result and return
  end

  ## only for doctors and pharmacists
  def get_medical_records
    result = {}
    med_rec_ids  = JSON.parse(@current_user[:extra_params_json])['mid']
    if med_rec_ids.present?
      result = TbMedicalRecords.get_med_records(med_rec_ids, @current_user['id'])
      if !result.present?
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_no_rec')
    end
    render :json => result and return
  end

end