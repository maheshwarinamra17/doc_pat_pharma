class DocumentController < ApplicationController

  before_action :authenticate_api_request

  def create_a_prescription
    result = {}
    if params['email_id'].present?
      if @current_user['role'] == APP_CONFIG['user_type']['doctor']
        params['doctor_user_id'] = @current_user['id']
        rand_stub = RandomStubs.new
        num_meds = rand(2..7)
        params['drugs_json'] = rand_stub.generate_random_prescription(num_meds).to_s  ## This should be provided by user in an actual system
        presc = TbPrescription.create_a_prescription(params)
        if presc.present? and presc[:id].present?
          result['id'] = presc[:id]
          result['patient_user_id'] = presc[:patient_user_id]
          result['drugs_json'] = JSON.parse(presc[:drugs_json])
          result['status'] = true
        else
          result['status'] = false
        end
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_patient')
    end
    render :json => result and return
  end

  def create_a_medical_record
    result = {}
    if params['email_id'].present?
      params['doc_access_json'] = {"source" => @current_user['id'], "others" =>{}}
      rand_stub = RandomStubs.new
      rand_test = rand_stub.generate_random_test
      params['med_test_type'] = rand_test['name'] ## In a real system this should contain med_test id
      params['med_test_json'] = rand_test.to_json.to_s
      med_rec = TbMedicalRecords.create_a_medical_record(params)
      if med_rec[:id].present?
        result['id'] = med_rec[:id]
        result['patient_user_id'] = med_rec[:patient_user_id]
        result['med_test_type'] = params['med_test_type']
        result['med_test_json'] = JSON.parse(params['med_test_json'])
        result['status'] = true
      else
        result['status'] = false
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_patient')
    end
    render :json => result and return
  end

  def request_a_prescription
    result = {}
    if params['id'].present? and params['patient_user_id'].present?
      if @current_user['role'] == APP_CONFIG['user_type']['doctor'] or @current_user['role'] == APP_CONFIG['user_type']['pharmacist']
        result = TbPrescription.request_a_prescription(params, @current_user)
        if !result.present?
          result = {}
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_rec')
        end
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_pat_pres')
    end
    render :json => result and return
  end

  def approve_a_prescription
    result = {}
    if params['id'].present? and params['request_user_id'].present?
      prescription = TbPrescription.find(params['id'])
      if @current_user['id'] == prescription['patient_user_id']
        result = TbPrescription.approve_a_prescription(params['request_user_id'], prescription)
        if !result.present?
          result = {}
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_appr')
        end
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_user_pres')
    end
    render :json => result and return
  end

  def request_a_med_record
    result = {}
    if params['id'].present? and params['patient_user_id'].present?
      if @current_user['role'] == APP_CONFIG['user_type']['doctor']
        result = TbMedicalRecords.request_a_med_rec(params,@current_user)
        if !result.present?
          result = {}
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_update')
        end
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_user_med')
    end
    render :json => result and return
  end

  def approve_a_med_record
    result = {}
    if params['id'].present? and params['request_user_id'].present?
      med_rec = TbMedicalRecords.find(params['id'])
      if @current_user['id'] == med_rec['patient_user_id']
        result = TbMedicalRecords.approve_a_med_rec(params['request_user_id'], med_rec)
        if !result.present?
          result = {}
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_update')
        end
      else
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_user_med')
    end
    render :json => result and return
  end


  def get_prescription_data
    if params['id'].present?
      result = TbPrescription.get_prescription_data(params,@current_user)
      col_name = :pharma_access
      if @current_user['role'] == APP_CONFIG['user_type']['doctor']
        col_name = :doc_access
      end
      if !result.present? or !result[col_name].present? or !(result[col_name] == APP_CONFIG['doc_status']['approved'])
        result = {}
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      else
        result[:drugs_json] = JSON.parse(result[:drugs_json])
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_rec')
    end
    render :json => result and return
  end

  def get_med_rec_data
    if params['id'].present?
      result = TbMedicalRecords.get_med_rec_data(params,@current_user)
      if !result.present? or !result[:doc_access_json].present? or !(result[:doc_access_json] == APP_CONFIG['doc_status']['approved'])
        result = {}
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_illegal')
      else
        result[:med_test_json] = JSON.parse(result[:med_test_json])
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_rec')
    end
    render :json => result and return
  end

  def get_prescriptions_by_patient
    result = {}
    if params['email_id'].present?
      user = TbUser.where(:email_id => params['email_id'])
      if user.first['role'] == APP_CONFIG['user_type']['patient']
        pres_ids = JSON.parse(user.first[:extra_params_json])['pid']
        if pres_ids.present? and pres_ids.kind_of?(Array)
          result['patient_user_id'] = user.first[:id]
          result['data'] = TbPrescription.where(:id => pres_ids).select(:id, :updated_at)
        else
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_no_rec')
        end
      else
        result = {}
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_patient')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_patient')
    end
    render :json => result and return
  end

  def get_med_rec_by_patient
    result = {}
    if params['email_id'].present?
      user = TbUser.where(:email_id => params['email_id'])
      if user.first['role'] == APP_CONFIG['user_type']['patient']
        pres_ids = JSON.parse(user.first[:extra_params_json])['mid']
        if pres_ids.present? and pres_ids.kind_of?(Array)
          result['patient_user_id'] = user.first[:id]
          result['data'] = TbMedicalRecords.where(:id => pres_ids).select(:id, :updated_at)
        else
          result['errors'] = Array.new
          result['errors'].push(I18n.t 'errors.e_no_rec')
        end
      else
        result = {}
        result['errors'] = Array.new
        result['errors'].push(I18n.t 'errors.e_patient')
      end
    else
      result['errors'] = Array.new
      result['errors'].push(I18n.t 'errors.e_patient')
    end
    render :json => result and return
  end



end