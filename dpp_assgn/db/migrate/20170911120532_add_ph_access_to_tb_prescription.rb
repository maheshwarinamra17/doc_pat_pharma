class AddPhAccessToTbPrescription < ActiveRecord::Migration[5.0]
  def change
    add_column(:tb_prescriptions, :pharma_access, :json)
  end
end
