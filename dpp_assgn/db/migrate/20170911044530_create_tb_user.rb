class CreateTbUser < ActiveRecord::Migration[5.0]
  def change
    create_table :tb_users do |t|
      t.string :name
      t.string :mobile
      t.string :email_id
      t.string :password_digest
      t.integer :role
      t.json :extra_params_json
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
