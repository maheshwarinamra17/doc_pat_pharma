class AddDocAccessToTbPrescription < ActiveRecord::Migration[5.0]
  def change
    add_column(:tb_prescriptions, :doc_access, :json)
  end
end
