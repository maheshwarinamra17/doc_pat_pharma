class CreateTbPrescription < ActiveRecord::Migration[5.0]
  def change
    create_table :tb_prescriptions do |t|
      t.integer :patient_user_id
      t.integer :doctor_user_id
      t.json :drugs_json
      t.json :extra_params_json
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
