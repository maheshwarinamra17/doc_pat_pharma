class CreateTbMedicalRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :tb_medical_records do |t|
      t.integer :patient_user_id
      t.string :med_test_type
      t.json :med_test_json
      t.json :doc_access_json
      t.datetime :created_at
      t.datetime :updated_at
    end
  end
end
