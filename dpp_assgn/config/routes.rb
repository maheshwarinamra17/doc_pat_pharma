Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root :to => "page#home"
  post 'api/authenticate', to: 'user#authenticate_user'
  post 'api/signup', to: 'user#create_a_new_user'
  post 'api/create_prescription', to: 'document#create_a_prescription'
  post 'api/create_medical_rec', to: 'document#create_a_medical_record'
  post 'api/request_prescription', to: 'document#request_a_prescription'
  post 'api/approve_prescription', to: 'document#approve_a_prescription'
  post 'api/request_med_record', to: 'document#request_a_med_record'
  post 'api/approve_med_record', to: 'document#approve_a_med_record'

  post 'api/get_req_prescriptions', to: 'user#get_prescriptions'
  post 'api/get_req_medical_records', to: 'user#get_medical_records'
  post 'api/get_prescription_data', to: 'document#get_prescription_data'
  post 'api/get_med_rec_data', to: 'document#get_med_rec_data'
  post 'api/get_prescriptions_by_patient', to: 'document#get_prescriptions_by_patient'
  post 'api/get_med_rec_by_patient', to: 'document#get_med_rec_by_patient'

end
